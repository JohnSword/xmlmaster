# README #

This is a simple XML socket server written in Java.
Check the .fla file to see how it works.
Supports:
- unlimited users (only limited by RAM and processing power since each user creates a new thread)
- private/public rooms
- private/public messages
- kicking users

# HOW TO USE #

start server using:
java XmlMaster 

server uses configfile.xml file

console messages:
HELP = lists console messages
QUIT = quits server
DEBUG = turns debug on and off
LIST = list users
ROOMS = list rooms
>[message] = broadcast a message to all users (ex: >Server will go down in 5 minutes)

<m admin="msg">[xml message]</m>

KICK [userid] = kick a user from the server. the client needs to process the kick message and close the connection:
<m admin="kick">[userid]</m>

PING = ping all clients and check for error status. client is deleted upon detection of error status. This is useful to flush clients 	that for some reason were not properly disconnected.

default messages (messages from server):

<l> = users list message, sends users list in the form of:
<users><u><i>userid</i><n>username</n></u></users>

<er [r=[roomid]]></e> = extended rooms list message, sends rooms list in the form of:

<erooms><r cid="[creatorid]" rid="[roomid]" nam="[roomname]" txt="[roomdescription]" on="[roomavailable]" grt="[greetingmssg]" kil="[roomkillable]" pas="[password]" shw="[roomvisible]" max="[maxusers]">[<u><i>[userids]</i></u>]</r></erooms>

If roomid is defined in message, the room extended info is returned in the form of:
<eroom><r cid="[creatorid]" rid="[roomid]" nam="[roomname]" txt="[roomdescription]" on="[roomavailable]" grt="[greetingmssg]" kil="[roomkillable]" pas="[password]" shw="[roomvisible]" max="[maxusers]">[<u><i>[userids]</i></u>]</r></eroom>

<s> = short rooms list message, sends rooms list in the form of:
<srooms><r on="[roomavailable]" rid="[roomid]" >[roomname]</r></srooms>

j = join message, user is added to room (port), sends user id and username in the form of:
<j><i>userid</i><n>username</n></j>

q = quit message, user is removed from room (port), sends user id 
in the form of:
<q>userid</q>


message commands:

<n>[username] = login message, sends username and retrieves id in the form of:
<j><i>userid</i><n>username</n></j>
throw error message: <err type="username"></err>

<l> = request users list, returns users list in the form of:
<users><u><i>userid</i><n>username</n></u></users>


custom messages:

<m [properties]="[property]">[xml message]</m>

broadcast message (to all but oneself):
<b fro="[senderuserid]" [properties]="[property]">[xml message]</b>

send message to specific user:
<m to="[targetuserid]" [properties]="[property]">[xml message]</m>

creates room and adds user as creator:
<r id="[userid]" n="[username]" rn="[roomname]" txt="[description]" max="[maxusers]" shw="[booleanshow]" kil="[booleankillable]" pas="[password]" on="[booleanon]" grt="[greetingmessage]"></r>


configuration file:
<server>
	<port>[Port Number]</port>
	<debug>[true/false]</debug>
	<access_log>[true/false]</access_log>
	<log>[logfilename]<log>
	<rooms>
		<r nam="[roomname]" txt="[roomdesc]" on="[1/0]" grt="[roomgreet]" kil="[1/0]" pas="" shw="[1/0]" max="[maxusers]" ></r>
	</rooms>
</server>


Room messages:

<rooms>
<r
  ([ct=int])
  [cid=str]
  [rid=str]
  [nam=str]
  [txt=str]
  [on=boolean]
  [grt=str]
  [kil=boolean]
  [pas=str]
  [shw=boolean]
  [max=int]
>
  Users List ...
  [<u><i>userId</i></u>]
</r>
</rooms>

Room Attributes
----------------------------------------------------------

Implied Attribute Description: 
 
(ct: the system time when the room was created.)
..........................................................


Required Attribute Description:

cid: The ID of the creator of the room. 
 
rid: The Room ID. This is the unique identifier for the room.

nam: A short, textual name for the room. 
Default: Room [Number]
..........................................................


Optional Attribute Description:

txt: A textual description or room data.
Default: No description
 
on: Indicates whether the room is currently available. Disabled rooms always reject requests for entry.
Default: 1
 
greet: A textual greeting to be used when new users enter a room. 
Default: No Greeting
 
kil: Indicates whether the room will be deleted after all of the participants leave. 1 means yes, 0 means no.
Default: 1
 
pas: Specifies the password required to gain access to the room. 
Default: No Password 
 
shw: Indicates whether the room is visible to clients. 1 means yes, 0 means no.
Default: 1

max: Indicates max number of users for the room.
Default: -1 (Unlimited)

specific Default room messages:
........................................................................

Room.Create Client created and entered a room by sending the room definition: 

<r id="[userid]" n="[username]" rn="[roomname]" txt="[description]" max="[maxusers]" shw="[booleanshow]" kil="[booleankillable]" pas="[password]" on="[booleanon]" grt="[greetingmessage]"></r>

Response messages:
Room.CreateAccepted: <rm typ="CreateAccepted" rid="[roomId]"></rm>
Room.CreateRejected: <rm typ="CreateRejected"></rm>
........................................................................

Room.Enter Client request to enter a room: 
<rm typ="Enter" uid="[userId]" rid="[roomId]" pas="[password]></rm>

Response messages:
Room.EnterAccepted: <rm typ="EnterAccepted" rid="[roomId]" uid="[userId]"></rm>
Room.EnterAccepted: <rm typ="EnterRejected" rid="[roomId]" uid="[userId]"></rm>
........................................................................

Room.Exit Client request to leave or be removed from a room: 
<rm typ="Exit" uid="[userId]" rid="[roomId]"></rm>

Response messages:
Room.ExitAccepted: <rm typ="ExitAccepted" rid="[roomId]" uid="[userId]"></rm>
........................................................................

Room.SetData Client request to replace the room's CustomData container: 
<rm typ="SetData" rid="[roomId]" uid="[userId]" nam="[roomname]" txt="[roomdescription]" on="[roomavailable]" grt="[greetingmssg]" kil="[roomkillable]" pas="[password]" shw="[roomvisible]" max="[maxusers]"></rm>

Response messages:
Room.SetDataAccepted: <rm typ="SetDataAccepted" rid="[roomId]"></rm>
Room.SetDataRejected: <rm typ="SetDataRejected" rid="[roomId]"></rm>