/*
 *  XmlMaster v1.93
 *  Date: 01/04/2004
 *  Copyright SitebySite 2004
 */

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class XmlMaster {

    static List<XmlMasterClientThread> clientList = new ArrayList<XmlMasterClientThread>();
    static List<XmlMasterRoom> roomList = new ArrayList<XmlMasterRoom>();
    static ConcurrentHashMap<String, String> usersLocation = new ConcurrentHashMap<String, String>();
    static ConcurrentHashMap<String, XmlMasterClientThread> usersIDList = new ConcurrentHashMap<String, XmlMasterClientThread>();
    static PrintWriter screenOut = new PrintWriter(System.out, true);
    static ServerSocket serverSocket = null;
    static Boolean debug;
    static Boolean uniqueUsers;
    static Boolean log;
    static String logfile;
    static XmlMasterConfig serverConfig;

    public static void main(String[] args) throws IOException {
	
	boolean listening = true;
	CommandReader tempCommand;
	Thread tempThread;
	XmlMasterClientThread tempClient;
	XmlMasterCleaner clientCleaner;
	//int currentNode;
	Integer tempPort;
	Boolean defaultRooms;
	Boolean loadDefault;
	
	screenOut.println("------------------------------------------------------");
	screenOut.println(" XmlMaster v1.91");
	screenOut.println(" Copyright 2003 - SitebySite");
	screenOut.println(" http://www.sitebysite.pt");
	screenOut.println("------------------------------------------------------");
	screenOut.println("  Type HELP to see the list of supported commands");
	screenOut.println("------------------------------------------------------");


	//get server configuration
	screenOut.println(" Using Configuration File: config.xml");
	try {
	    //load xml file
	    serverConfig = new XmlMasterConfig("config.xml");
	    screenOut.println(" " + serverConfig.txtconfig + "\n");
	} catch (Exception e) {
	    screenOut.println(" Failed reading Configuration File: config.xml");
	    System.exit(-1);
	}

	if (serverConfig.readStatus) {
	    //set server config
	    tempPort = new Integer(serverConfig.port);
	    //check for debug flag
	    debug = (boolean)serverConfig.debug;
	    loadDefault = (boolean)serverConfig.loadrooms;
	    defaultRooms = (boolean)serverConfig.DefaultRooms;
	    uniqueUsers = (boolean)serverConfig.uniquesers;
	    log = (boolean)serverConfig.accesslog;
	    logfile = serverConfig.logfile;
	    //if load default rooms is set
	    if (loadDefault.booleanValue()) {
		//if default rooms defined build default rooms
		int roomsize = serverConfig.defaultRoomList.size();
		if (defaultRooms.booleanValue()) {
		    for (int i = 0; i < roomsize; i++) {
			String data = String.valueOf(serverConfig.defaultRoomList.get(i));
			XmlMaster.createRoom(data, "", "");
		    }
		}
	    }


	    try {
		serverSocket = new ServerSocket(tempPort.intValue());
	    } catch (IOException e) {
		System.err.println("Could not listen on port: " + tempPort.toString());
		System.exit(-1);
	    }

	    boolean chk = debug.booleanValue();

	    if (chk) {
		screenOut.println(" Debug: true\n");
	    } else {
		screenOut.println(" Debug: false\n");
	    }
	    screenOut.println(" Server started and listening on port " + tempPort.toString());
	    screenOut.println("------------------------------------------------------" + "\n");
	    screenOut.print(">");
	    screenOut.flush();
	    //initiate server command reader
	    tempCommand = new CommandReader();
	    tempCommand.start();
	    //initiate room cleaner
	    clientCleaner = new XmlMasterCleaner();
	    tempThread = new Thread(clientCleaner);
	    tempThread.start();
	    while (listening) {
		try {
		    tempClient = new XmlMasterClientThread(serverSocket.accept());
		    //adds new user
		    //clientList.add(tempClient);
		    tempThread = new Thread(tempClient);
		    tempThread.start();
		    //screenOut.println(tempClient.userID);
		    //Calendar rightNow = Calendar.getInstance();					
		} catch (IOException e) {
		}
	    }
	    serverSocket.close();
	} else {
	    screenOut.println(" Error: Failed to start server.");
	    System.exit(-1);
	}
    }

//server pipe functionality
    //send message to all users
    static void sendToAllClients(String message) {
	int i;
	XmlMasterClientThread foo;
	//XmlMasterClientThread tempClient;
	//long start = System.currentTimeMillis();
	// do operation to be timed here
	int loopnum = clientList.size();
	for (i = loopnum; --i >= 0;) {
	    foo = (XmlMasterClientThread) clientList.get(i);
	    foo.send(message);
	}
	//long time = System.currentTimeMillis() - start;
	//XmlMaster.screenOut.println(String.valueOf(time));
	message = "";
    }

    //sends message to specified user
        /*
    static void sendTo(String message,String to){
    long start = System.currentTimeMillis();
    int i;
    XmlMasterClientThread foo;
    int loopnum = clientList.size();
    for (i = loopnum; --i >= 0; ){
    foo = (XmlMasterClientThread)clientList.get(i);
    if (to.equalsIgnoreCase(foo.userID)){
    foo.send(message);
    break;
    }
    }
    message = "";
    long time = System.currentTimeMillis() - start;
    XmlMaster.screenOut.println("SendToOld: "+String.valueOf(time));
    }
     */
    //sends message to specified user        
    static void sendTo(String message, String to) {
	//long start = System.currentTimeMillis();
	XmlMasterClientThread foo;
	foo = (XmlMasterClientThread) XmlMaster.usersIDList.get(to);
	foo.send(message);
	message = "";
	//long time = System.currentTimeMillis() - start;
	//XmlMaster.screenOut.println("SendToSpecial: "+String.valueOf(time));
    }

    //sends message to all but specified user
    static void sendBroadcast(String message, String notto) {
	int i;
	XmlMasterClientThread foo;
	//XmlMasterClientThread tempClient;
	int loopnum = clientList.size();
	for (i = loopnum; --i >= 0;) {
	    foo = (XmlMasterClientThread) clientList.get(i);
	    if (!notto.equalsIgnoreCase(foo.userID)) {
		foo.send(message);
	    }
	}
	message = "";
    }

    //send client list message to user
    static void sendClientList(String to) {
	int i;
	String Users;
	XmlMasterClientThread foo;
	Users = "<users>";
	int loopnum = clientList.size();
	for (i = loopnum; --i >= 0;) {
	    foo = (XmlMasterClientThread) clientList.get(i);
	    //Users += "<u><i>" + foo.userID + "</i><n>" + foo.Username + "</n></u>";
	    Users += "<u><i>" + foo.userID + "</i><n><![CDATA[" + foo.Username + "]]></n></u>";
	}
	Users += "</users>\u0000";
	XmlMaster.sendTo(Users, to);
    }

    //send short room list message to user
    static void sendShortRoomList(String to) {
	int i;
	String Rooms;
	XmlMasterRoom foo;
	Rooms = "<srooms>";
	int loopnum = roomList.size();
	for (i = loopnum; --i >= 0;) {
	    foo = (XmlMasterRoom) roomList.get(i);
	    //show if room visibility flag is true
	    if (foo.Visible.intValue() == 1) {
		Rooms += "<r on=\"" + foo.Available + "\" rid=\"" + foo.roomId + "\">" + foo.roomName + "</r>";
	    }
	}
	Rooms += "</srooms>\u0000";
	XmlMaster.sendTo(Rooms, to);
    }

    //send extended room info message to user
    static void sendExtRoomList(String to, String roomid) {
	int i;
	String Rooms;
	XmlMasterRoom foo;
	Rooms = "<eroom>";
	int loopnum = roomList.size();
	for (i = loopnum; --i >= 0;) {
	    foo = (XmlMasterRoom) roomList.get(i);
	    //show if room visibility flag is true
	    if (foo.Visible.intValue() == 1) {
		if (foo.roomId.equalsIgnoreCase(roomid)) {
		    Rooms += foo.getRoomData();
		}
	    }
	}
	Rooms += "</eroom>\u0000";
	XmlMaster.sendTo(Rooms, to);
    }

    //send extended room list message to user
    static void sendExtRoomsList(String to) {
	int i;
	String Rooms;
	XmlMasterRoom foo;
	Rooms = "<erooms>";
	int loopnum = roomList.size();
	for (i = loopnum; --i >= 0;) {
	    foo = (XmlMasterRoom) roomList.get(i);
	    //show if room visibility flag is true
	    if (foo.Visible.intValue() == 1) {
		Rooms += foo.getRoomData();
	    }
	}
	Rooms += "</erooms>\u0000";
	XmlMaster.sendTo(Rooms, to);
    }

//server functionality
    //delete user
    static synchronized void deleteClient(XmlMasterClientThread deadClient) {
	int num;
	num = clientList.indexOf(deadClient);
	//XmlMasterClientThread foo;
	//foo = (XmlMasterClientThread)clientList.get(num);
	//check if user is in a room
	int i;
	XmlMasterRoom rooms;
	//String userroom = String.valueOf(usersLocation.get(foo.userID));
	String userroom = String.valueOf(usersLocation.get(deadClient.userID));
	if (!userroom.equalsIgnoreCase("root")) {
	    //user is in a room, check wich one
	    int loopnum = roomList.size();
	    for (i = loopnum; --i >= 0;) {
		rooms = (XmlMasterRoom) roomList.get(i);
		if (userroom.equalsIgnoreCase(rooms.roomId)) {
		    //found the room
		    //check if server will delete room
		    if (rooms.roomclientList.size() == 1) {
			//user is the last user in room
			//check if server can delete room
			if (rooms.Killable.intValue() == 1) {
			    printToConsole(" Room " + rooms.roomId + " was closed.");
			    //delete room and thats it
			    roomList.remove(i);
			    break;
			} else {
			    //room was not deleted, server will delete user from room users list
			    rooms.removeUser(deadClient.userID);
			    break;
			}
		    } else {
			//user is not the last user, delete user
			rooms.removeUser(deadClient.userID);
			break;
		    }
		}
	    }
	}
	//broadcast quit message
	try {
	    //broadcast end connection from user
	    sendBroadcast("<q>" + deadClient.userID + "</q>\u0000", deadClient.userID);
	    //delete user from userLocation hash
	    usersLocation.remove(deadClient.userID);
	    usersIDList.remove(deadClient.userID);
	    //delete user
	    clientList.remove(num);
	} catch (Exception e) {
	}
	printToConsole(" Client " + (num + 1) + ": " + deadClient.userID + " - " + deadClient.Username + " - " + deadClient.clientIP + " has left");
    }

    //creates a new room
    static void createRoom(String createdata, String userid, String ipport) {
	XmlMasterRoom tempRoom;
	tempRoom = new XmlMasterRoom(createdata, userid, ipport);
	roomList.add(tempRoom);
	tempRoom.start();
	printToConsole(" Created Room: " + tempRoom.roomName + " Room creator: " + tempRoom.creatorName + " Room creatorid: " + tempRoom.creatorId);
    }

    //try to put a user into a room
    static synchronized void enterRoom(String roomId, String userId, String password, String ipport) {
	int i;
	boolean foundroom = false;
	XmlMasterRoom rooms;
	//check which room user is trying to enter
	int loopnum = roomList.size();
	for (i = loopnum; --i >= 0;) {
	    rooms = (XmlMasterRoom) roomList.get(i);
	    if (roomId.equalsIgnoreCase(rooms.roomId)) {
		//found the room
		rooms.addUser(userId, password, ipport);
		foundroom = true;
		break;
	    }
	}
	//couldnt find room send reject message
	if (!foundroom) {
	    XmlMaster.sendTo("<rm typ=\"EnterRejected\" rid=\"" + roomId + "\"></rm>\u0000", userId);
	}
    }

    //remove user from room
    static synchronized void exitRoom(String roomId, String userId) {
	int i;
	boolean foundroom = false;
	XmlMasterRoom rooms;
	//check which room user is trying to exit
	int loopnum = roomList.size();
	for (i = loopnum; --i >= 0;) {
	    rooms = (XmlMasterRoom) roomList.get(i);
	    if (roomId.equalsIgnoreCase(rooms.roomId)) {
		//found the room
		rooms.removeUser(userId);
		//check if server will delete room
		if (rooms.roomclientList.isEmpty()) {
		    //user was the last user in room
		    //check if server can delete room
		    if (rooms.Killable.intValue() == 1) {
			printToConsole(" Room " + rooms.roomId + " was closed.");
			//delete room and thats it
			roomList.remove(i);
		    }
		}

		foundroom = true;
		break;
	    }
	}
	//couldnt find room send reject message
	if (!foundroom) {
	    XmlMaster.sendTo("<rm typ=\"ExitRejected\" rid=\"" + roomId + "\"></rm>\u0000", userId);
	}
    }

    //kick user from room
    static void kickUserFromRoom(String roomId, String userId, String IpPort) {
	int i;
	boolean foundroom = false;
	XmlMasterRoom rooms;
	//check which room user is trying to exit
	int loopnum = roomList.size();
	for (i = loopnum; --i >= 0;) {
	    rooms = (XmlMasterRoom) roomList.get(i);
	    if (roomId.equalsIgnoreCase(rooms.roomId)) {
		//found the room
		//call kick message
		rooms.kickUser(userId, IpPort);
		break;
	    }
	}
    }

    static synchronized void setRoomData(String roomId, String userId, String roomName, String Description, String Available, String Greeting, String Killable, String Password, String Visible, String MaxUsers, String IpPort) {
	int i;
	boolean foundroom = false;
	XmlMasterRoom rooms;
	//find the room
	//printToConsole(roomName);
	int loopnum = roomList.size();
	for (i = loopnum; --i >= 0;) {
	    rooms = (XmlMasterRoom) roomList.get(i);
	    if (roomId.equalsIgnoreCase(rooms.roomId)) {
		//found the room
		//check if admin
		if (IpPort.equalsIgnoreCase(rooms.AdminIpPort)) {
		    //change the data
		    rooms.roomName = roomName;
		    rooms.Description = Description;
		    rooms.Available = new Integer(Available);
		    rooms.Greeting = Greeting;
		    rooms.Killable = new Integer(Killable);
		    rooms.Password = Password;
		    rooms.Visible = new Integer(Visible);
		    rooms.MaxUsers = new Integer(MaxUsers);
		    foundroom = true;
		}
		break;
	    }
	}
	//couldnt find room send reject message
	if (!foundroom) {
	    XmlMaster.sendTo("<rm typ=\"SetDataRejected\" rid=\"" + roomId + "\"></rm>\u0000", userId);
	} else {
	    //data was changed successfully
	    XmlMaster.sendTo("<rm typ=\"SetDataAccepted\" rid=\"" + roomId + "\"></rm>\u0000", userId);
	}
    }

//console functionality
    static void printToConsole(String txt) {
	boolean chk = debug.booleanValue();
	if (chk) {
	    screenOut.print(txt + "\n");
	    screenOut.print(">");
	    screenOut.flush();
	}
	txt = "";
    }

    static void listClients() {
	int i;
	XmlMasterClientThread foo;
	printToConsole(" Client List: ");
	int loopnum = clientList.size();
	for (i = loopnum; --i >= 0;) {
	    foo = (XmlMasterClientThread) clientList.get(i);
	    printToConsole(" Client " + i + ": " + foo.Username + " - Id: " + foo.userID + " - Ip: " + foo.clientIP + " - Loc: " + usersLocation.get(foo.userID));
	}
    }

    //list rooms
    static void listRooms() {
	int i;
	XmlMasterRoom foo;
	printToConsole(" Room List: ");
	int loopnum = roomList.size();
	for (i = loopnum; --i >= 0;) {
	    foo = (XmlMasterRoom) roomList.get(i);
	    printToConsole("Room " + i + ": " + foo.roomName + " - Id: " + foo.roomId + " - Creator: " + foo.creatorName + " - CreatorID: " + foo.creatorId);
	    //printToConsole(foo.getRoomData());
	}
    }

    //ping clients and delete erroneous clients
    static void pingClients() {
	int i;
	XmlMasterClientThread foo;
	int loopnum = clientList.size();
	for (i = loopnum; --i >= 0;) {
	    foo = (XmlMasterClientThread) clientList.get(i);
	    try {
		// get Client connection status
		foo.out.print('\u0000');
		foo.out.flush();
		String errorstatus = String.valueOf(foo.out.checkError());
		errorstatus = errorstatus.trim().toUpperCase();
		//if error status is true delete user
		if (errorstatus.compareTo("TRUE") == 0) {
		    printToConsole(foo.userID);
		    foo.killClient();
		}
	    } catch (Exception e) {
		//deleteClient(foo);
		printToConsole("Error when reading");
	    }
	}
    }

    //check for erroneous rooms and delete them
    static void pingRooms() {
	//this doesnt detect if room is on but is gone
	int i;
	XmlMasterRoom foo;
	int loopnum = roomList.size();
	for (i = loopnum; --i >= 0;) {
	    foo = (XmlMasterRoom) roomList.get(i);
	    if (foo.creatorName == null && "root".equals(foo.creatorId)) {
		roomList.remove(i);
		printToConsole("Room " + foo.roomId + " was deleted");
	    }
	}
    }

    static boolean checkUsername(String username, String id) {
	int i;
	XmlMasterClientThread foo;
	int loopnum = clientList.size();
	boolean nameerror = false;
	for (i = loopnum; --i >= 0;) {
	    foo = (XmlMasterClientThread) clientList.get(i);
	    if (username.compareTo(foo.Username) == 0) {
		if (id.compareTo(foo.userID) == 0) {
		    nameerror = false;
		} else {
		    nameerror = true;
		}
	    }
	}
	return nameerror;
    }
}

class XmlMasterClientThread extends Thread {
    //PrintWriter screenOut = new PrintWriter(System.out, true);

    private Socket socket = null;
    private long rndId;
    public String IpPort;
    public String clientIP;
    public String userID;
    public String Username;
    public PrintWriter policyOut;
    public PrintStream out;
    public BufferedReader in;

    public XmlMasterClientThread(Socket socket) {
	super("XmlMasterClientThread");
	this.socket = socket;
	clientIP = socket.getInetAddress().getHostAddress();
	IpPort = String.valueOf(clientIP + ":" + socket.getPort());
	userID = "";
    }

    @Override
    public void run() {
	try {
	    policyOut = new PrintWriter(socket.getOutputStream(), true);
	    out = new PrintStream(socket.getOutputStream(), true);
	    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	    XmlMaster.printToConsole(" Client connection: " + clientIP);

	    String inputLine = "";
	    int streamResult;
	    char buf[] = new char[1];

	    do {
		do {
		    streamResult = in.read(buf, 0, 1);
		    inputLine += buf[0];
		} while (buf[0] != '\u0000');

		//catch empty stream, will not send it
		String chkxml = inputLine.substring(0, 1);

		if (chkxml.compareTo("<") == 0) {
		    //catch xml commands
		    chkxml = inputLine.substring(0, 3);

		    // send policy file to flash if requested
		    if (chkxml.compareTo("<po") == 0) {
			socket.setSoTimeout(10000);
			XmlMaster.printToConsole(" Send policy file to: " + clientIP);
			policyOut.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?><cross-domain-policy xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"http://www.adobe.com/xml/schemas/PolicyFileSocket.xsd\"><allow-access-from domain=\"*\" to-ports=\""+XmlMaster.serverConfig.port+"\" secure=\"false\" /></cross-domain-policy>\u0000");
			policyOut.flush();
			streamResult = -1;
		    }
		    
		    //login message
		    if (chkxml.compareTo("<n>") == 0) {
			//check for clients with error status
			XmlMaster.pingClients();
			XmlMaster.pingRooms();
			//build user id
			rndId = Math.round(Math.random() * 100000000);
			userID = String.valueOf(rndId);
			// check for unique id
			if(XmlMaster.usersIDList.containsKey(userID))
			{
			    //send error message to user
			    XmlMaster.sendTo("<err type=\"userid\"></err>", clientIP);
			    //delete user
			    logOff();
			}
			XmlMaster.printToConsole(" Client Login: " + userID + " - " + clientIP);
			XmlMaster.usersIDList.put(userID, this);
			 //adds new user
			XmlMaster.clientList.add(this);
			//set username
			Username = inputLine.substring(3);
			Username = Username.trim();
			//check user name availability
			if (XmlMaster.uniqueUsers.booleanValue()) {
			    if (XmlMaster.checkUsername(Username, userID)) {
				//send error message to user
				XmlMaster.sendTo("<err type=\"username\"></err>", clientIP);
				//delete user
				logOff();
			    }
			}
			//send login message
			//XmlMaster.sendToAllClients("<j><i>" + userID + "</i><n>" + Username + "</n></j>\u0000");
			XmlMaster.sendToAllClients("<j><i>" + userID + "</i><n><![CDATA[" + Username + "]]></n></j>\u0000");
			//put user in default room
			XmlMaster.usersLocation.put(userID, "root");
			//XmlMaster.usersIPList.put(userID,this);
			inputLine = "";
		    }
		    
		    //check for request and send users list
		    if (chkxml.compareTo("<l>") == 0) {
			XmlMaster.sendClientList(userID);
			inputLine = "";
		    }

		    //check for request and send short rooms list
		    if (chkxml.compareTo("<s>") == 0) {
			XmlMaster.sendShortRoomList(userID);
			inputLine = "";
		    }

		    //check for request and send extended rooms list
		    if (chkxml.compareTo("<er") == 0) {
			//check for room id property
			chkxml = inputLine.substring(0, 5);
			if (chkxml.compareTo("<er r") == 0) {
			    try {
				XMLElement XML = new XMLElement();
				XML.parseString(inputLine, 0);
				XmlMaster.sendExtRoomList(userID, XML.getProperty("r"));
			    } catch (XMLParseException e) {
				XmlMaster.printToConsole(" Error. Could not parse message: "+inputLine+" "+e.toString());
			    }
			} else {
			    XmlMaster.sendExtRoomsList(userID);
			}
			inputLine = "";
		    }

		    if (chkxml.compareTo("<b ") == 0) {
			//process broadcast message
			try {
			    XMLElement XML = new XMLElement();
			    XML.parseString(inputLine, 0);
			    if (XML.getProperty("fro") != null) {
				//send to all less the user
				XmlMaster.sendBroadcast(inputLine, XML.getProperty("fro"));
				inputLine = "";
			    }
			} catch (XMLParseException e) {
			    XmlMaster.printToConsole(" Error. Could not parse message: "+inputLine+" "+e.toString());
			}
		    }

		    //create room message
		    if (chkxml.compareTo("<r ") == 0) {
			//delete erroneous rooms
			XmlMaster.pingRooms();
			//call build room
			XmlMaster.createRoom(inputLine, userID, IpPort);
			inputLine = "";
		    }

		    if (chkxml.compareTo("<rm") == 0) {
			//manage room messages
			try {

			    XMLElement XML = new XMLElement();
			    XML.parseString(inputLine, 0);

			    //enter room request message
			    if (XML.getProperty("typ").equalsIgnoreCase("Enter")) {
				XmlMaster.enterRoom(XML.getProperty("rid"), XML.getProperty("uid"), XML.getProperty("pas"), IpPort);
			    }
			    //exit client message
			    if (XML.getProperty("typ").equalsIgnoreCase("Exit")) {
				XmlMaster.exitRoom(XML.getProperty("rid"), XML.getProperty("uid"));
			    }
			    // Client request to replace the room's CustomData
			    if (XML.getProperty("typ").equalsIgnoreCase("SetData")) {
				XmlMaster.setRoomData(XML.getProperty("rid"), XML.getProperty("uid"), XML.getProperty("nam"), XML.getProperty("txt"), XML.getProperty("on"), XML.getProperty("grt"), XML.getProperty("kil"), XML.getProperty("pas"), XML.getProperty("shw"), XML.getProperty("max"), IpPort);
			    }
			    // Client request to kick a user from a room
			    if (XML.getProperty("typ").equalsIgnoreCase("Kick")) {
				//XmlMaster.printToConsole(IpPort);
				//todo:
				//send a message to the room requesting to kick the user with id XML.getProperty("uid")
				XmlMaster.kickUserFromRoom(XML.getProperty("rid"), XML.getProperty("uid"), IpPort);
			    }
			    inputLine = "";
			} catch (XMLParseException e) {
			    XmlMaster.printToConsole(" Error. Could not parse message: "+inputLine+" "+e.toString());
			}
		    }

		    if (chkxml.compareTo("<m ") == 0) {
			//manage normal messages
			//check for "to" property
			chkxml = inputLine.substring(0, 5);
			if (chkxml.compareTo("<m to") == 0) {
			    try {
				XMLElement XML = new XMLElement();
				XML.parseString(inputLine, 0);
				//XmlMaster.sendTo(inputLine,XML.getProperty("to"));
				XmlMaster.sendTo(inputLine, XML.getProperty("to"));
			    } catch (XMLParseException e) {
				XmlMaster.printToConsole(" Error. Could not parse message: "+inputLine+" "+e.toString());
			    }
			    inputLine = "";
			} else {
			    XmlMaster.sendToAllClients(inputLine);
			}
		    }
		}
		//empty the message
		inputLine = "";

	    } while (streamResult != -1);
	    //this.finalize();
	    logOff();
	} catch (IOException e) {
	    //this.finalize();
	    //e.printStackTrace();
	    //something went wrong here and the user connection could have been severed
	    //try and delete erroneous user
	    //check for clients with error status
	    //XmlMaster.pingClients();
	    //XmlMaster.pingRooms();
	    logOff();
	    //out.close();
	    //this.stop();
	}
    }
    
    private void logOff ()
    {
	try {
	    if(!userID.isEmpty()) killClient();
	    in.close();
	    out.close();
	    socket.close();
	    this.interrupt();
	} catch (IOException socketexception) {
	    XmlMaster.printToConsole(" Error. Failed log off. "+socketexception.toString());
	}
    }
    
    public void finalize() throws Throwable {
	super.finalize();
	try {
	    killClient();
	    in.close();
	    socket.close();
	} catch (IOException socketexception) {
	    //yield();
	}
    }

    public void killClient() {
	XmlMaster.deleteClient(this);
    }

    public void send(String message) {
	//if its valid xml send message to users
	if (message.length() > 3) {
	    try {
		out.print(message);
		//out.print('\u0000');
		out.flush();
		XmlMaster.printToConsole(" " + userID + " >>> " + message);
	    } catch (Exception e) {
		return;
	    }
	//XmlMaster.printToConsole(String.valueOf(out.checkError()));
	}
    }
}