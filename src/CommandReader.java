
import java.io.*;

class CommandReader extends Thread {

    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    PrintWriter screenOut = new PrintWriter(System.out, true);
    String command;
    String checkbroadcast;

    public CommandReader() {
	super("CommandReader");
    }

    @Override
    public void run() {
	boolean valid;
	try {
	    while ((command = in.readLine()) != null) {
		valid = false;

		if (command.length() > 0) {
		    checkbroadcast = command.substring(0, 1);
		    if (checkbroadcast.compareTo(">") == 0) {
			valid = true;
			String msg = command.substring(1);
			msg = msg.trim();
			//XmlMaster.printToConsole(msg);
			screenOut.println(" Admin broadcast message: \"" + msg + "\"\n");
			//send admin message
			XmlMaster.sendToAllClients("<m admin=\"msg\">" + msg + "</m>\u0000");
		    }
		    command = command.trim().toUpperCase();
		    if (command.length() > 3) {
			checkbroadcast = command.substring(0, 4);
			if (checkbroadcast.compareTo("KICK") == 0) {
			    valid = true;
			    String userid = command.substring(4).trim();
			    screenOut.println(" Admin kicks user: \"" + userid + "\"\n");
			    //send kick message
			    XmlMaster.sendTo("<m admin=\"kick\">" + userid + "</m>\u0000", userid);
			}
			if (checkbroadcast.compareTo("PING") == 0) {
			    valid = true;
			    XmlMaster.printToConsole("Pinging Clients\n");
			    XmlMaster.pingClients();
			}
		    }
		    command = command.trim().toUpperCase();
		    if (command.compareTo("QUIT") == 0) {
			valid = true;
			XmlMaster.serverSocket.close();
			System.exit(0);
		    }
		    if (command.compareTo("LIST") == 0) {
			valid = true;
			screenOut.println("");
			XmlMaster.listClients();
			screenOut.println("");
		    }
		    if (command.compareTo("HELP") == 0) {
			valid = true;
			screenOut.println("\n LIST - lists all currently connected clients");
			screenOut.println(" QUIT - disconnects all clients and exits");
			screenOut.println(" DEBUG - switches message printing to console on/off");
			screenOut.println(" ROOMS - lists all rooms");
			screenOut.println(" >[message] - broadcast a message to all users");
			screenOut.println(" KICK [userid] - kick the specified user");
			screenOut.println(" PING - ping clients\n");
		    }
		    if (command.compareTo("DEBUG") == 0) {
			valid = true;
			boolean chk = XmlMaster.debug.booleanValue();
			screenOut.println("");
			if (chk) {
			    //XmlMaster.debug = new Boolean("false");
			    XmlMaster.debug = false;
			    screenOut.println("Debug is inactive.");
			} else {
			    //XmlMaster.debug = new Boolean("true");
			    XmlMaster.debug = true;
			    screenOut.println("Debug is active.");
			}
			screenOut.println("");
		    }
		    if (command.compareTo("ROOMS") == 0) {
			valid = true;
			screenOut.println("");
			XmlMaster.listRooms();
			screenOut.println("");
		    }
		}
		if (!valid) {
		    screenOut.println("\nError: Not a supported command\n");
		}
		screenOut.print(">");
		screenOut.flush();
	    }
	} catch (IOException e) {
	    screenOut.print(e.toString());
	}

    }
};