import java.util.ArrayList;
import java.util.List;

class XmlMasterRoom extends Thread {
    
	public List<String> roomclientList = new ArrayList<String>();
	public String roomName;
	public String roomId;
	public String creatorId;
	public String creatorName;
	public String Description = "";
	public String Greeting = "";
	public String Password = "";
	public Integer Available = new Integer(1);
	public Integer Killable = new Integer(1);
	public Integer Visible = new Integer(1);
	public Integer MaxUsers = new Integer(-1);
        public String AdminIpPort;
	private long rndId;
	private String roomDataXML;
	private List<String> KickList = new ArrayList<String>();
	private List<String> roomUsersIpPort = new ArrayList<String>();

	public XmlMasterRoom(String createdata,String userid,String ipport){
		super("XmlMasterRoom");
		try{
			XMLElement XML = new XMLElement();
			XML.parseString(createdata,0);
			//Required Attribute Descriptions:
			this.roomName = XML.getProperty("rn");
			this.creatorId = XML.getProperty("id");
			this.creatorName = XML.getProperty("n");
                        this.AdminIpPort = ipport;

			//optional Attribute Description:
			if (XML.getProperty("txt") != null){
				this.Description = XML.getProperty("txt");
			}
			if (XML.getProperty("on") != null){
				this.Available = new Integer(XML.getProperty("on"));
			} 
			if (XML.getProperty("grt") != null){
				this.Greeting = XML.getProperty("grt");
			}
			if (XML.getProperty("kil") != null){
				this.Killable = new Integer(XML.getProperty("kil"));
			}
			if (XML.getProperty("pas") != null){
				this.Password = XML.getProperty("pas");
			}
			if (XML.getProperty("shw") != null){
				this.Visible = new Integer(XML.getProperty("shw"));
			}
			if (XML.getProperty("max") != null){
				this.MaxUsers = new Integer(XML.getProperty("max"));
			}
		}catch(XMLParseException e){
			XmlMaster.printToConsole(" Error creating room.");
			this.Available = new Integer(0);
			this.Visible = new Integer(0);
			this.creatorId = "root";
			XmlMaster.sendTo("<rm typ=\"CreateRejected\"></rm>\u0000",userid);
		}
	}

    @Override
	public void run(){
		//build room id
		rndId = Math.round(Math.random() * 100000000);
		roomId= String.valueOf("rid"+rndId);
		//add user to roomlist
		roomclientList.add(creatorId);
                //add ip port to list
                //the first one represents the room admin
                roomUsersIpPort.add(AdminIpPort);
		//update user location
		XmlMaster.usersLocation.put(creatorId,roomId);
		XmlMaster.sendTo("<rm typ=\"CreateAccepted\" rid=\""+roomId+"\"></rm>\u0000",creatorId);
		//build a function to delete erronious rooms
	}

	public synchronized void addUser(String userid,String password,String ipport){		
                //first check if user is in ban list
                int inlist = KickList.indexOf(ipport);
                boolean userenter = false;
                if(inlist != -1){
                    //user is banned from room                 
                } else {
                    //check if its possible to enter this room               
                    if (Available.intValue() == 1 && Visible.intValue() == 1 && roomclientList.size() != MaxUsers.intValue()){
                            if (Password.length() == 0){
                                    //no password was defined for this room
                                    roomclientList.add(userid);
                                    roomUsersIpPort.add(ipport);
                                    //update user location
                                    XmlMaster.usersLocation.put(userid,roomId);
                                    //send new room user message
                                    roomBroadcast("<rm typ=\"EnterAccepted\" rid=\""+roomId+"\" uid=\""+userid+"\"></rm>\u0000");
                                    userenter = true;
                            } else {
                                    //check if user entered the correct password
                                    if (Password.equalsIgnoreCase(password)){
                                            roomclientList.add(userid);
                                            roomUsersIpPort.add(ipport);
                                            //update user location
                                            XmlMaster.usersLocation.put(userid,roomId);
                                            //send new room user message
                                            roomBroadcast("<rm typ=\"EnterAccepted\" rid=\""+roomId+"\" uid=\""+userid+"\"></rm>\u0000");
                                            userenter = true;
                                    }
                            }
                    }
                }
		//couldnt addUser send reject message
		if (!userenter){
			XmlMaster.sendTo("<rm typ=\"EnterRejected\" rid=\""+roomId+"\"></rm>\u0000",userid);
		}
	}

	public synchronized void removeUser(String userid){
		//get user number from roomclientList
		int clientListNum = roomclientList.indexOf(userid);                
		//first check if user is admin
		if (creatorId.equalsIgnoreCase(userid)){
			//if user is admin
			//and user not last user
			if (roomclientList.size() > 1){
                            //send exit message to room users
                            roomBroadcast("<rm typ=\"ExitAccepted\" rid=\""+roomId+"\" uid=\""+userid+"\"></rm>\u0000");
                            roomclientList.remove(clientListNum);
                            roomUsersIpPort.remove(clientListNum);
                            //set second user to admin
                            creatorId = String.valueOf(roomclientList.get(0));
                            AdminIpPort = String.valueOf(roomUsersIpPort.get(0));
                            //put user to root
                            XmlMaster.usersLocation.put(userid,"root");
                        } else {
				//make root as admin, that means room is empty
				creatorId = "root";
				//clear room of users
				roomclientList.remove(clientListNum);
                                roomUsersIpPort.remove(clientListNum);
                                AdminIpPort = "";
				XmlMaster.sendTo("<rm typ=\"ExitAccepted\" rid=\""+roomId+"\" uid=\""+userid+"\"></rm>\u0000",userid);
			}
		} else {
			//send exit message to room users
			roomBroadcast("<rm typ=\"ExitAccepted\" rid=\""+roomId+"\" uid=\""+userid+"\"></rm>\u0000");
			//user is not admin, just delete user
			roomclientList.remove(clientListNum);
                        roomUsersIpPort.remove(clientListNum);
			XmlMaster.usersLocation.put(userid,"root");
		}
	}

	public void roomBroadcast(String message){
		//send message to room users
		int i;
		int loopnum = roomclientList.size();
                for (i = loopnum; --i >= 0; ){
		//for (i=0;i<loopnum;++i){
			XmlMaster.sendTo(message,String.valueOf(roomclientList.get(i)));
		} 
	}

        public void kickUser(String userid, String ipport){
            //check if user requesting the kick action is admin
            AdminIpPort = String.valueOf(roomUsersIpPort.get(0));
            if (AdminIpPort.equalsIgnoreCase(ipport)){
                //user is admin
                //add ipport to list of banned ips
                int clientListNum = roomclientList.indexOf(userid);
                KickList.add(roomUsersIpPort.get(clientListNum));                
                //kick user id
                removeUser(userid);
            }
        }
        
  	public String getRoomData(){
		roomDataXML = "<r ";
		roomDataXML += "cid=\""+creatorId+"\" ";
		roomDataXML += "rid=\""+roomId+"\" ";
		roomDataXML += "nam=\""+roomName+"\" ";
		roomDataXML += "txt=\""+Description+"\" ";
		roomDataXML += "on=\""+Available+"\" ";
		roomDataXML += "grt=\""+Greeting+"\" ";
		roomDataXML += "kil=\""+Killable+"\" ";
		roomDataXML += "pas=\""+Password+"\" ";
		roomDataXML += "shw=\""+Visible+"\" ";
		roomDataXML += "max=\""+MaxUsers+"\" ";
		roomDataXML += ">";
		roomDataXML += "<u>";
		int i;
		int loopnum = roomclientList.size();
		for (i=0;i<loopnum;++i){
			roomDataXML += "<i>"+roomclientList.get(i)+"</i>";
		}
		roomDataXML += "</u>";
		roomDataXML += "</r>";
    		return roomDataXML;
 	}
}