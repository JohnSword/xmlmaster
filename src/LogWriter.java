
import java.io.*;
import java.util.*;

/*
 * LogWriter.java
 *
 *
 * Copyright (C) 2001 Neeld Tanksley
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/**
 *
 *
 * The LogWriter 
 *
 * @author Neeld Tanksley
 * @version .91
 */
public class LogWriter {

    String logDir;
    String accessLogName;
    String errorLogName;
    boolean logAccess;
    boolean logError;

    public LogWriter(String logDir,
	    String accessLogName,
	    boolean logAccess,
	    String errorLogName,
	    boolean logError) {

	this.logDir = logDir;
	this.logAccess = logAccess;
	this.logAccess = logError;

	this.accessLogName = accessLogName;
	this.errorLogName = errorLogName;

	this.getAccesslog();
	this.getErrorlog();
    }

    /**
     *This method verifys or creates an access log
     */
    public final boolean getAccesslog() {

	boolean blFolderExists = false;
	boolean blFileExists = false;
	File LogFolder;
	File AccessLog;
	FileWriter accessLog;

	LogFolder = new File(logDir);
	blFolderExists = LogFolder.exists();

	if (!blFolderExists) {
	    try {
		LogFolder.mkdirs();
	    } catch (Exception e) {
		logError(e);
		System.out.println("Could not create logs dir");
	    }
	}

	AccessLog = new File(logDir + accessLogName);
	blFileExists = AccessLog.exists();

	if (!blFileExists) {
	    try {
		AccessLog.createNewFile();
	    } catch (IOException e) {
		logError(e);
		System.out.println("Could not create access.log");
	    }
	}

	return true;
    }

    /**
     *This method writes to an access log
     */
    public void addAccessLog(String logText) {
	if (logAccess) {
	    Calendar cal = Calendar.getInstance();
	    try {
		FileWriter FWaccessLog = new FileWriter(logDir + accessLogName, true);
		FWaccessLog.write(cal.getTime() + " | " + logText + "\r\n");
		FWaccessLog.close();
	    } catch (IOException e) {
		logError(e);
		System.out.println("Could not open access.log");
	    } catch (Exception e) {
		System.out.println(e);
	    }
	}
    }

    /**
     *This method verifys or creates an error log
     */
    public final boolean getErrorlog() {
	boolean blFolderExists = false;
	boolean blFileExists = false;
	File LogFolder;
	File ErrorLog;
	FileWriter errorLog;

	LogFolder = new File(logDir);
	blFolderExists = LogFolder.exists();

	if (!blFolderExists) {
	    try {
		LogFolder.mkdirs();
	    } catch (Exception e) {
		System.out.println("Could not create logs dir");
	    }
	}

	ErrorLog = new File(logDir + errorLogName);
	blFileExists = ErrorLog.exists();

	if (!blFileExists) {
	    try {
		ErrorLog.createNewFile();
	    } catch (IOException e) {
		System.out.println("Could not create error.log");
	    }
	}

	return true;
    }

    /**
     *This method writes to an error log
     */
    public void logError(Exception e) {
	System.out.print("called Error log");
	Calendar cal = Calendar.getInstance();
	try {
	    FileWriter FWaccessLog = new FileWriter(logDir + errorLogName, true);
	    FWaccessLog.write(System.getProperty("line.separator") + cal.getTime().toString() + System.getProperty("line.separator"));
	    e.printStackTrace(new PrintWriter(FWaccessLog));
	    FWaccessLog.write(System.getProperty("line.separator") + "_________________________________________________________");
	    FWaccessLog.close();
	} catch (IOException err) {
	    System.out.println("Could not open error.log");
	}
    }

    public boolean getLogAccess() {
	return this.logAccess;
    }
}