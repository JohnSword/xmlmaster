
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public final class XmlMasterConfig {

    //public Vector defaultRoomList = new Vector();
    public List<XMLElement> defaultRoomList = new ArrayList<XMLElement>();
    public boolean readStatus = false;
    public int port;
    public boolean debug;
    public boolean accesslog;
    public boolean uniquesers;
    public boolean loadrooms;
    public boolean DefaultRooms;
    public String logfile;
    public String txtconfig;
    private XMLElement xserverConfig = new XMLElement();

    public XmlMasterConfig(String confFile) {

	xserverConfig = loadConf(confFile);

	//get all the children
	//Vector childNodes = xserverConfig.getChildren();
	List<XMLElement> childNodes = xserverConfig.getChildren();
	txtconfig = "{";

	//Now check for any child nodes
	int loopnum = childNodes.size();
	for (int i = 0; i < loopnum; i++) {
	    XMLElement fooNode = (XMLElement) childNodes.get(i);
	    String fooNodeName = fooNode.getTagName();
	    String fooNodeValue = fooNode.getContents();


	    //read configuration properties
	    if (fooNodeName.equalsIgnoreCase("port")) {
		port = Integer.valueOf(fooNodeValue).intValue();
		txtconfig += "Port=" + fooNodeValue + ", ";
	    }
	    if (fooNodeName.equalsIgnoreCase("debug")) {
		debug = StringToBol(fooNodeValue);
		txtconfig += "Debug=" + fooNodeValue + ", ";
	    }
	    if (fooNodeName.equalsIgnoreCase("accesslog")) {
		accesslog = StringToBol(fooNodeValue);
		txtconfig += "AccessLog=" + fooNodeValue + ", ";
	    }
	    if (fooNodeName.equalsIgnoreCase("logfile")) {
		logfile = fooNodeValue;
		txtconfig += "LogFile=" + fooNodeValue + ", ";
	    }
	    if (fooNodeName.equalsIgnoreCase("uniqusers")) {
		uniquesers = StringToBol(fooNodeValue);
		txtconfig += "UniqueUsers=" + fooNodeValue + ", ";
	    }
	    if (fooNodeName.equalsIgnoreCase("loadrooms")) {
		loadrooms = StringToBol(fooNodeValue);
		txtconfig += "LoadRooms=" + fooNodeValue;
	    }
	    if (fooNodeName.equalsIgnoreCase("rooms")) {
		DefaultRooms = true;
		//build default rooms
		defaultRoomList = fooNode.getChildren();
	    }
	}
	txtconfig += "}";
    }

    public XMLElement loadConf(String confFile) {
	XMLElement foo = new XMLElement();
	String xml = "";
	try {
	    //String currentdir = System.getProperty("user.dir");
	    //confFile = currentdir+"\\"+confFile;
	    InputStream in = new FileInputStream(confFile);
	    while (in.available() > 0) {
		byte[] b = new byte[in.available()];
		in.read(b);
		xml = xml + new String(b);
	    }
	    in.close();
	    foo.parseString(xml, 0);
	    readStatus = true;
	} catch (FileNotFoundException e) {
	    XmlMaster.printToConsole("Could not find configuration file: config.xml");
	    System.exit(-1);
	} catch (IOException e) {
	    //e.printStackTrace(System.out);
	    XmlMaster.printToConsole("Could not find configuration file: config.xml");
	    System.exit(-1);
	}
	return foo;
    }

    public boolean StringToBol(String txtbol) {

	if (txtbol.equalsIgnoreCase("true")) {
	    return true;
	}
	return false;
    }
}