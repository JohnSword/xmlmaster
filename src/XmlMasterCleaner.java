/*
 * XmlMasterCleaner.java
 *
 * Created on March 27, 2004, 7:46 PM
 */

/**
 *
 * @author  Administrator
 */
class XmlMasterCleaner extends Thread {

    /** Creates a new instance of XmlMasterCleaner */
    public XmlMasterCleaner() {
	super("XmlMasterCleaner");
    }

    @Override
    public void run() {
	while (true) {
	    try {
		//1 min = 1000ms * 60
		int sleeptime = 60000; //1 minute
		sleep(sleeptime);
		//clean users and rooms then return to sleep
		cleanUp();
	    } catch (Exception e) {
	    }
	}
    }

    void cleanUp() {
	XmlMaster.pingClients();
	XmlMaster.pingRooms();
    }
}
